package webApp.objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class BookContentActivities {

    public WebDriver driver;

    public BookContentActivities(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Header

    @FindBy(xpath = "//div[starts-with(@class, 'NavBarTop_arrowIcon')]")
    private WebElement backButton;

    //Content

    @FindBy(xpath = "//div[starts-with(@class, 'ActivitiesList_activitiesBox')]//following::li")
    private List<WebElement> activitiesList;


    public List<WebElement> getActivitiesList() {
        return activitiesList;
    }

    public WebElement getBackButton() {
        return backButton;
    }
}
