package webApp.objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Index {

    public WebDriver driver;

    public Index(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    //header

    @FindBy(xpath = "//div[starts-with(@class, 'NavBarTop_arrowIcon')]")
    private WebElement backButton;

    @FindBy(xpath = "//div[starts-with(@class, 'NavBarTop_arrowIcon')]//following::p[1]")
    private WebElement booksubtitle;

    @FindBy(xpath = "//div[starts-with(@class, 'NavBarTop_arrowIcon')]//following::p[2]")
    private WebElement booktitle;

    @FindBy(xpath = "//div[starts-with(@class, 'FloatMenu_popBox')]")
    private WebElement optionsButton;

    //Content

    @FindBy(xpath = "//div[starts-with(@class, 'UnitIndex_unitContent')]")
    private List<WebElement> unitLIst;

    @FindBy(xpath = "//li[starts-with(@class, 'SectionIndex_sectionIndex')]")
    private List<WebElement> sectionList;

    @FindBy(xpath = "//div[starts-with(@class, 'SectionIndex_sectionButton')]")
    private List<WebElement> sectionButtonList;

    public WebElement getBackButton() {
        return backButton;
    }

    public WebElement getBooksubtitle() {
        return booksubtitle;
    }

    public WebElement getBooktitle() {
        return booktitle;
    }

    public WebElement getOptionsButton() {
        return optionsButton;
    }

    public List<WebElement> getUnitLIst() {
        return unitLIst;
    }

    public List<WebElement> getSectionList() {
        return sectionList;
    }

    public List<WebElement> getSectionButtonList() {
        return sectionButtonList;
    }
}
