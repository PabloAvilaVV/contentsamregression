package webApp.objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Dashboard {

    public WebDriver driver;

    public Dashboard(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Header

    @FindBy(id = "QaIconEdubook")
    private WebElement imageLogo;

    @FindBy(id = "QaBtnGoToAcademy")
    private WebElement academiaButton;

    @FindBy(xpath = "//div[starts-with(@class, 'Header_avatar')]")
    private WebElement userButton;

    @FindBy(xpath = "//div[starts-with(@class, 'Header_menuIcon')]")
    private WebElement optionsButton;

    //Books dashboard

    @FindBy(xpath = "//li[starts-with(@id, 'QaBookDashboard')]")
    private List<WebElement> bookList;

    @FindBy(xpath = "//div[contains(@class, 'AddButton_addbutton')]")
    private List<WebElement> addBook;

    public WebElement getImageLogo() {
        return imageLogo;
    }

    public WebElement getAcademiaButton() {
        return academiaButton;
    }

    public WebElement getUserButton() {
        return userButton;
    }

    public WebElement getOptionsButton() {
        return optionsButton;
    }

    public List<WebElement> getBookList() {
        return bookList;
    }

    public List<WebElement> getAddBook() {
        return addBook;
    }
}
