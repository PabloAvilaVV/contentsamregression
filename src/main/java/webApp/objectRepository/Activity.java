package webApp.objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Activity {

    public WebDriver driver;

    public Activity(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Header

    @FindBy(xpath = "//div[starts-with(@class, 'NavBarTop_arrowIcon')]")
    private WebElement backButton;

    @FindBy(xpath = "//div[starts-with(@class, 'NavBarTop_arrowIcon')]//following::p[1]")
    private WebElement title;

    @FindBy(xpath = "//div[starts-with(@class, 'NavBarTop_arrowIcon')]//following::p[2]")
    private WebElement subTitle;

    @FindBy(xpath = "//div[starts-with(@class, 'FloatMenu_popBox')]")
    private WebElement optionsButton;

    //Content

    @FindBy(xpath = "//button[starts-with(@class, 'Activity_nextButton')]")
    private WebElement rightArrow;

    @FindBy(xpath = "//div[starts-with(@class, 'Activity_activityContent')]")
    private WebElement activity;

    //--------------------------------------------------------------------------------------------

    public WebElement getBackButton() {
        return backButton;
    }

    public WebElement getRightArrow() {
        return rightArrow;
    }

    public WebElement getTitle() {
        return title;
    }

    public WebElement getSubTitle() {
        return subTitle;
    }

    public WebElement getOptionsButton() {
        return optionsButton;
    }

    public WebElement getActivity() {
        return activity;
    }
}
