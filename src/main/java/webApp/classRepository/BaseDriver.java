package webApp.classRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseDriver extends DriverActions{

    private static Properties prop;
    private static FileInputStream fis;

    public static WebDriverWait waiter;
    public static WebDriverWait plusSizeWaiter;
    public static WebDriver driver;

    public static WebDriver newBornDriver() throws IOException {

        prop = new Properties();
        fis = new FileInputStream("./configurations/driver.properties");
        prop.load(fis);

        String navigator = prop.getProperty("navigator");

        switch (navigator) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver");
                driver = new ChromeDriver();

                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
                driver = new FirefoxDriver();
                break;
        }

        waiter = new WebDriverWait(driver, 15);
        plusSizeWaiter = new WebDriverWait(driver, 30);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

}
