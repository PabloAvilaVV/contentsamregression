Feature: The user navigates throught activities sometimes it's not posible to see the activity and the spinner does not desappear
  #https://vicensvivesdigital.atlassian.net/browse/EBON-1216

  Scenario Outline: The user navigates forward throught the activities
    Given I enter to Edubook WebApp with the username <username> and password <password>
    When I enter to the <bookNumber>th book on the book index
    And I expand the <unitNumber>th unit on the unit index
    And I click on the <activityNumberButton> on the unit dropdown
    And I click on the activityNumber <activityNumber> on the activities list on the book content
    And I click on the right arrow <times> times on the activityView/MauthorView
    Then I should see the three dots button on the activitiesView/MAuthorView
    And I should see the activityView/MauthorView on the activity view
    When I click on the arrow/ExitButton on the activitiesView/MauthorView
    Then I should see the activities list on the book content

    Examples:
      | username                             | password    | bookNumber | unitNumber | activityNumberButton | activityNumber | times |
      | studentCalificador1                  | edubook2020 | 0          | 0          | 2                    | 0              | 17    |
      | SamElementNavigator@test.com         | edubook2020 | 0          | 0          | 4                    | 0              | 6     |
      | StudentSAMNavigation                 | edubook2020 | 0          | 0          | 5                    | 0              | 8     |