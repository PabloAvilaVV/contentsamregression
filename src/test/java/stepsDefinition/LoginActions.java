package stepsDefinition;

import io.cucumber.java.en.Given;
import org.openqa.selenium.support.ui.ExpectedConditions;
import webApp.classRepository.BaseDriver;
import webApp.objectRepository.LoginPage;

import java.io.IOException;

public class LoginActions extends BaseDriver {

    @Given("^I enter to Edubook WebApp with the username ([^\"]*) and password ([^\"]*)$")
    public void iEnterToEdubookWebAppWithTheUsernameUsernameAndPasswordPassword(String username, String password) throws IOException, InterruptedException {
        driver = newBornDriver();
        driver.get("http://edubook.vicensvives.com/es/inicio");

        LoginPage loginPage = new LoginPage(driver);
        waiter.until(ExpectedConditions.elementToBeClickable(loginPage.getUsername()));
        loginPage.getUsername().sendKeys(username);
        loginPage.getPassword().sendKeys(password);
        Thread.sleep(500);
        loginPage.getSubmitButton().click();

    }


}
