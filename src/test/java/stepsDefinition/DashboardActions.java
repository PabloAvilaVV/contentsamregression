package stepsDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.support.ui.ExpectedConditions;
import webApp.classRepository.BaseDriver;
import webApp.objectRepository.Dashboard;
import webApp.objectRepository.LoginPage;

import java.io.IOException;

public class DashboardActions extends BaseDriver {

    @When("^I enter to the ([^\"]*)th book on the book index$")
    public void iEnterToTheBookNumberThBookOnTheBookIndex(String bookNumber) {

        int bookNumberAux = Integer.parseInt(bookNumber);
        Dashboard dashboard = new Dashboard(driver);
        waiter.until(ExpectedConditions.elementToBeClickable(dashboard.getImageLogo()));
        dashboard.getBookList().get(bookNumberAux).click();
    }
}
