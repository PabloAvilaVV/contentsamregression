package stepsDefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import webApp.classRepository.BaseDriver;
import webApp.objectRepository.BookContentActivities;

public class BookContentActivitiesActions extends BaseDriver {
    @And("^I click on the activityNumber ([^\"]*) on the activities list on the book content$")
    public void iClickOnTheActivityNumberActivityNumberOnTheActivitiesListOnTheBookContent(String activityNumber) {

        int activityNumberAux = Integer.parseInt(activityNumber);

        BookContentActivities bookContentActivities = new BookContentActivities(driver);
        waiter.until(ExpectedConditions.elementToBeClickable(bookContentActivities.getBackButton()));
        bookContentActivities.getActivitiesList().get(activityNumberAux).click();
    }

    @Then("^I should see the activities list on the book content$")
    public void iShouldSeeTheActivitiesListOnTheBookContent() {
        BookContentActivities bookContentActivities = new BookContentActivities(driver);
        Assert.assertTrue(bookContentActivities.getActivitiesList().size()>0);
    }
}
