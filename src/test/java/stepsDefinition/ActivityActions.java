package stepsDefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import webApp.classRepository.BaseDriver;
import webApp.objectRepository.Activity;

public class ActivityActions extends BaseDriver {
    @And("^I click on the right arrow ([^\"]*) times on the activityView\\/MauthorView$")
    public void iClickOnTheRightArrowTimesTimesOnTheActivityViewMauthorView(String times) throws InterruptedException {
        int timesAux = Integer.parseInt(times);
        Activity activity;

        for (int i=0; i<timesAux; i++){
            activity = new Activity(driver);
            waiter.until(ExpectedConditions.elementToBeClickable(activity.getBackButton()));
            activity.getRightArrow().click();
            Thread.sleep(1000);
        }


    }

    @Then("^I should see the three dots button on the activitiesView\\/MAuthorView$")
    public void iShouldSeeTheThreeDotsButtonOnTheActivitiesViewMAuthorView() {
        Activity activity = new Activity(driver);
        Assert.assertTrue(activity.getOptionsButton().isDisplayed());
    }


    @And("^I should see the activityView\\/MauthorView on the activity view$")
    public void iShouldSeeTheActivityViewMauthorViewOnTheActivityView() {
        Activity activity = new Activity(driver);
        Assert.assertTrue(activity.getActivity().isDisplayed());
    }

    @When("^I click on the arrow\\/ExitButton on the activitiesView\\/MauthorView$")
    public void iClickOnTheArrowExitButtonOnTheActivitiesViewMauthorView() {
        Activity activity = new Activity(driver);
        activity.getBackButton().click();
    }

    @And("^I click on the right arrow ([^\"]*) times on the activityView\\/MauthorView refreshing the page every step$")
    public void iClickOnTheRightArrowTimesTimesOnTheActivityViewMauthorViewRefreshingThePageEveryStep(String times) throws InterruptedException {
        int timesAux = Integer.parseInt(times);
        Activity activity;

        for (int i=0; i<timesAux; i++){
            driver.navigate().refresh();
            activity = new Activity(driver);
            waiter.until(ExpectedConditions.elementToBeClickable(activity.getBackButton()));
            activity.getRightArrow().click();
            Thread.sleep(1000);
        }
    }
}
