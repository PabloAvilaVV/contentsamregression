package stepsDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.support.ui.ExpectedConditions;
import webApp.classRepository.BaseDriver;
import webApp.objectRepository.LoginPage;

import java.io.IOException;
import java.util.Set;

public class GeneralEdubookActions extends BaseDriver {

    @Given("I open Edubook webApp")
    public void iOpenEdubookWebApp() throws IOException {

        driver = newBornDriver();
        driver.get("http://edubook.vicensvives.com/es/inicio");

        LoginPage loginPage = new LoginPage(driver);
        waiter.until(ExpectedConditions.visibilityOf(loginPage.getImageLogo()));

    }

    @When("I refresh the browser")
    public void iRefreshTheBrowser() throws InterruptedException {
        Thread.sleep(3000);
        driver.navigate().refresh();
        Thread.sleep(500);
    }
}
