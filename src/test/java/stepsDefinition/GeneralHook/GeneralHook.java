package stepsDefinition.GeneralHook;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import webApp.classRepository.BaseDriver;

public class GeneralHook extends BaseDriver {

//    @Before
//    public void setUpConfiguration(Scenario scenario){
//
//
//    }

    @After
    public void tearDownConfiguration(Scenario scenario) throws InterruptedException {
        System.out.println("Clossing drivers....");
        Thread.sleep(5000);

        driver.close();
        driver.quit();
    }

}
