package stepsDefinition;

import io.cucumber.java.en.And;
import org.openqa.selenium.support.ui.ExpectedConditions;
import webApp.classRepository.BaseDriver;
import webApp.objectRepository.Index;

public class IndexActions extends BaseDriver {
    @And("^I expand the ([^\"]*)th unit on the unit index$")
    public void iExpandTheUnitNumberThUnitOnTheUnitIndex(String unitNumber) {
        int unitNumberAux = Integer.parseInt(unitNumber);
        Index index = new Index(driver);
        waiter.until(ExpectedConditions.elementToBeClickable(index.getOptionsButton()));
        index.getUnitLIst().get(unitNumberAux).click();
    }

    @And("^I click on the ([^\"]*) on the unit dropdown$")
    public void iClickOnTheActivityNumberButtonOnTheUnitDropdown(String activityButtonNumber) {
        int activityButtonNumberAux = Integer.parseInt(activityButtonNumber);
        Index index = new Index(driver);
        waiter.until(ExpectedConditions.elementToBeClickable(index.getOptionsButton()));
        index.getSectionButtonList().get(activityButtonNumberAux).click();
    }
}
